@if(!empty($files))
    @foreach($files as $file)
        <li class="list-group-item p-3">
            {{$file}}
            <button class="edit_file btn btn-secondary float-right" data-file="{{$file}}">Edit</button>
        </li>
    @endforeach
@else
    <p class="alert alert-warning">No saved files for this user.</p>
@endif