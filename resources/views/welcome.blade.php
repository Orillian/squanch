@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col text-center bg-white rounded">
            <h3>Squanchitizer -<small>"Squanch" all the things</small></h3>
                <img src="{{ asset('./images/squanchy.jpg') }}" alt="">
            <p class="alert alert-info col-6 offset-3 mt-3"> Login to make all sorts of squanchy text!</p>
        </div>
    </div>
@endsection