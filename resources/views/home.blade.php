@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Squanchitizer</div>

                    <div class="card-body">
                        <label>Write something below</label>
                        <textarea id="text-squancher" class="w-100 form-control mb-2"></textarea>
                        <button class="save_string btn btn-primary">Save File</button>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-header">User Files.</div>
                    <div class="card-body" id="file_list">
                       @include('layouts.file_list')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('early')
    <script>
        var saveRoute = '{{route('save')}}';
        var editRoute = '{{route('edit')}}';
    </script>
@endpush
