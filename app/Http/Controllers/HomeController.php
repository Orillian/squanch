<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userFolder = Auth::user()->name;
        $files = Storage::disk('public')->files($userFolder);

        return view('home')->with(['files' => $files]);
    }

    public function save(Request $request)
    {
        $request->validate([
                               'text' => 'required|string',
                           ]);
        $userFolder = Auth::user()->name;
        $filename = Str::uuid().'.txt';

        $text = $request->input('text');
        File::put(Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix().$userFolder.'/'.$filename, $text);

        $files = Storage::disk('public')->files($userFolder);

        $view = View::make('layouts.file_list')->with(['files' => $files])->render();

        return Response::json(
            [
                'view' => $view,
            ]);
    }

    public function edit(Request $request)
    {
        $request->validate([
                               'file_name' => 'required|string',
                           ]);

        $content = File::get(Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix().$request->input('file_name'));
        return Response::json(
            [
                'text' => $content,
            ]);
    }
}
